//
//  DataManager.swift
//  LiquiTrack
//
//  Created by Student on 13/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataManager{
    // Singleton
    public static let instance = DataManager()
    private init(){}
    
    public var drinks = [Drink]()
    private let typeKey = "type"
    private let volumeKey = "volume"
    private let dateKey = "timestamp"
    private var hasChanged = false
    
    public var HasDataChanged : Bool {
        get { return hasChanged }
    }
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func addDrinkOf(type : String, with volume : Float){
        let context = getContext()
        
        let entity = NSEntityDescription.entity(forEntityName: "Drink", in: context)
        let drink = Drink(entity : entity!, insertInto: context)
        
        drink.type = type
        drink.volume = volume
        drink.timestamp = Date().timeIntervalSinceReferenceDate

        // Save the data changes
        do {
            try context.save()
            print("Drink saved!")
            hasChanged = true
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func removeDrink(_ drink : Drink){
        getContext().delete(drink);
        
        // Save the data changes
        do {
            try getContext().save()
            print("Drink deleted!")
            hasChanged = true
        } catch let error as NSError  {
            print("Could not delete \(error), \(error.userInfo)")
        }
    }
    
    func printDrinks(_ drinks : [Drink]){
        print ("Result Count: \(drinks.count)")
        for result in drinks {
            
            if let type = result.type {
                print("\(result.volume)ml of \(type)")
            }
            //Date formatting
            let date = Date.init(timeIntervalSinceReferenceDate: result.timestamp)
            let dateString = DateFormatter.localizedString(from: date, dateStyle: .short, timeStyle: .short)
            print("Date: \(dateString)")
        }
    }
    
    func fetchTodaysDrinks() -> [Drink] {
        //create a fetch request, telling it about the entity
        
        let fetchRequest: NSFetchRequest<Drink> = Drink.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: true)]
        var todaysDrinks = [Drink]()
        
        do {
            //fetching the results
            todaysDrinks = try getContext().fetch(fetchRequest)
            
            while(todaysDrinks.count > 0 && !todaysDrinks[0].isToday){
                todaysDrinks.removeFirst()
            }
            
            print("Fetched todays drinks")
            hasChanged = false
        } catch let error as NSError {
            print("Error with request: \(error)")
        }
        return todaysDrinks
    }
    
    func updateDrinks(){
        //create a fetch request, telling it about the entity
        let fetchRequest: NSFetchRequest<Drink> = Drink.fetchRequest()
        
        do {
            //fetching the results
            drinks = try getContext().fetch(fetchRequest)
            hasChanged = false
        } catch let error as NSError {
            print("Error with request: \(error)")
        }
    }
    
    func fetchDrinksGroupedByDay() -> [[Drink]]{
        updateDrinks()
        
        var sections = [[Drink]]()
        var index = 0
        for drink in drinks{
            if sections.count <= index {
                sections.append([drink])
            }
            else if sections[index][0].day == drink.day{
                    sections[index].append(drink)
                }
            else{
                index += 1
                sections.append([drink])
            }
        }
        return sections
    }
}
