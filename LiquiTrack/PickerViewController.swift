//
//  PickerViewController.swift
//  LiquiTrack
//
//  Created by Jordan Pichler on 05/03/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import UIKit
import UserNotifications

class PickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var intervalPicker: UIPickerView!
    
    let center = UNUserNotificationCenter.current()
    var pickerDataSource = ["every hour", "every 2 hours", "every 3 hours", "every 4 hours", "every 5 hours", "never"];
    var selected: Int {
        var value = UserDefaults.standard.integer(forKey: "reminderInterval")
        
        if value == 0 {
            value = 5
        } else {
            value = value - 1
        }
        
        return value
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intervalPicker.dataSource = self;
        self.intervalPicker.delegate = self;
        
        intervalPicker.selectRow(selected, inComponent: 0, animated: false)
        
    }
    
    func numberOfComponents(in: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let defaults = UserDefaults.standard
        
        switch row {
        case 0:
            defaults.set(1, forKey: "reminderInterval")
            rescheduleNotfications(inInterval: 1)
            
        case 1:
            defaults.set(2, forKey: "reminderInterval")
            rescheduleNotfications(inInterval: 2)
            
        case 2:
            defaults.set(3, forKey: "reminderInterval")
            rescheduleNotfications(inInterval: 3)
            
        case 3:
            defaults.set(4, forKey: "reminderInterval")
            rescheduleNotfications(inInterval: 4)
            
        case 4:
            defaults.set(5, forKey: "reminderInterval")
            rescheduleNotfications(inInterval: 5)
            
        case 5:
            defaults.set(0, forKey: "reminderInterval")
            rescheduleNotfications(inInterval: 0)
            
        default:
            defaults.set(0, forKey: "reminderInterval")
            rescheduleNotfications(inInterval: 0)
            
        }
        
    }
    
    func rescheduleNotfications(inInterval interval: Int) {
        if interval == 0 {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["regularReminder"])
        } else {
            let defaults = UserDefaults.standard
            
            let content = UNMutableNotificationContent()
            let userName = defaults.string(forKey: "userName")
            content.body = "Hey \(userName!), time to drink some water!"
            content.sound = UNNotificationSound.default()
            
            var fromQuietHour = defaults.integer(forKey: "quietFrom")
            var untilQuietHour = defaults.integer(forKey: "quietUntil")
            
            if fromQuietHour == 0 {
                fromQuietHour = 24
            }
            
            if untilQuietHour == 0 {
                untilQuietHour = 24
            }
            
            var awakeHours = fromQuietHour - untilQuietHour
            
            if awakeHours < 0 {
                awakeHours = awakeHours + 24
            }
            
            if fromQuietHour == 24 {
                fromQuietHour = 0
            }
            
            if untilQuietHour == 24 {
                untilQuietHour = 0
            }
            
            let dailyNotifications: Int = awakeHours / interval
        
            let intervalSeconds = Double(interval) * 3600
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: intervalSeconds, repeats: true)
            
            //Create the request
            let request = UNNotificationRequest(
                identifier: "regularReminder",
                content: content,
                trigger: trigger
            )
            
            //Schedule the request
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            
        }
    }
}
