//
//  SettingsViewController.swift
//  LiquiTrack
//
//  Created by Jordan Pichler on 16/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import UIKit

class SettingsViewController:  UITableViewController  {
    
    @IBOutlet var shortcutActionCells: [UITableViewCell]!
    @IBOutlet var reminderCells: [UITableViewCell]!
    @IBOutlet var userInfoCells: [UITableViewCell]!
    
    
    override func viewWillAppear(_ animated: Bool) {
        let defaults = UserDefaults.standard
        
        // Fill User Info Section
        let userName = defaults.string(forKey: "userName")!
        let intakeGoal = defaults.integer(forKey: "intakeGoal")
        
        userInfoCells[0].detailTextLabel!.text = "\(userName)"
        userInfoCells[1].detailTextLabel!.text = "\(intakeGoal) ml"
        
        
        // Fill Reminders Section
        let interval = defaults.integer(forKey: "reminderInterval")
        let quietTime = defaults.string(forKey: "quietTime")
        
        if interval == 0 {
            reminderCells[0].detailTextLabel!.text = "never"
        } else if interval == 1 {
            reminderCells[0].detailTextLabel!.text = "every hour"
        } else {
            reminderCells[0].detailTextLabel!.text = "every \(interval) hours"
        }
        reminderCells[1].detailTextLabel!.text = quietTime

        
        // Fill 3D-Touch Options Section
        let typeLow = defaults.string(forKey: "typeLow")!
        let typeMedium = defaults.string(forKey: "typeMedium")!
        let typeHigh = defaults.string(forKey: "typeHigh")!
        
        let amountLow = defaults.integer(forKey: "amountLow")
        let amountMedium = defaults.integer(forKey: "amountMedium")
        let amountHigh = defaults.integer(forKey: "amountHigh")
        
        shortcutActionCells[0].detailTextLabel!.text = "\(amountLow) ml of \(typeLow)"
        shortcutActionCells[1].detailTextLabel!.text = "\(amountMedium) ml of \(typeMedium)"
        shortcutActionCells[2].detailTextLabel!.text = "\(amountHigh) ml of \(typeHigh)"
        
        if tableView.indexPathForSelectedRow != nil {
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let defaults = UserDefaults.standard

        if (indexPath.section == 0 && indexPath.row == 0) {
            let alertController = UIAlertController(title: "Name", message: "Enter your name", preferredStyle: .alert)
            
            alertController.addTextField { (textField) in
                textField.placeholder = "eg. Jordan"
            }
            
            let okAction = UIAlertAction(title: "OK", style: .default) { action in
                let textField = alertController.textFields![0] as UITextField
                print("Text field: \(textField.text)")
                defaults.set(textField.text, forKey: "userName")
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true)
        }
        
        if (indexPath.section == 0 && indexPath.row == 1) {
            let alertController = UIAlertController(title: "Daily Goal", message: "Enter your daily fluid intake goal (in milliliters)", preferredStyle: .alert)
            
            alertController.addTextField { (textField) in
                textField.placeholder = "eg. 2000 ml"
                textField.keyboardType = .numberPad
            }
            
            let okAction = UIAlertAction(title: "OK", style: .default) { action in
                let textField = alertController.textFields![0] as UITextField
                print("Text field: \(textField.text)")
                defaults.set(Int(textField.text!)!, forKey: "intakeGoal")
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            //Show alert view
            present(alertController, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("Preparing")

        if segue.identifier == "amountLowOption" {
            print("Low")
            let touchOptionIdentifier = 1
            let destinationVC = segue.destination as! QuickDrinkViewController
            destinationVC.touchOptionIdentifier = touchOptionIdentifier
            
        } else if segue.identifier == "amountMediumOption" {
            print("Medium")

            let touchOptionIdentifier = 2
            let destinationVC = segue.destination as! QuickDrinkViewController
            destinationVC.touchOptionIdentifier = touchOptionIdentifier
            
        } else if segue.identifier == "amountHighOption" {
            print("High")

            let touchOptionIdentifier = 3
            let destinationVC = segue.destination as! QuickDrinkViewController
            destinationVC.touchOptionIdentifier = touchOptionIdentifier
        }
        
        
    }

    
    
}
