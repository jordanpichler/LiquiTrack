//
//  LineChartManager.swift
//  LiquiTrack
//
//  Created by Student on 15/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import Foundation
import Charts

// Replace it with a bar chart?
class LineChartManager{
    var chartColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
    var limitColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
    var chart: LineChartView!
    var values = [ChartDataEntry]()
    var days = [String]()
    
    // Defenition of the number format
    var numberFormat : NumberFormatter {
        get{
            let pFormatter: NumberFormatter = NumberFormatter()
            pFormatter.numberStyle = NumberFormatter.Style.percent
            pFormatter.maximumFractionDigits = 2
            pFormatter.multiplier = 1.0
            pFormatter.percentSymbol = " l"
            return pFormatter
        }
    }
    
    init(_ chart : LineChartView){
        self.chart = chart
        
        // Colors
        chart.drawBordersEnabled = false
        chart.xAxis.axisLineColor = chartColor
        chart.xAxis.labelTextColor = chartColor
        
        chart.leftAxis.drawAxisLineEnabled = false
        chart.leftAxis.labelTextColor = chartColor

        chart.rightAxis.drawAxisLineEnabled = false
        
        // Offsets & Legend/Description Disable
        chart.extraLeftOffset = 10
        chart.extraRightOffset = 20
        chart.extraBottomOffset = 10
        chart.legend.enabled = false
        chart.chartDescription?.enabled = false
        
        // Removes the background grid
        chart.leftAxis.drawGridLinesEnabled = false
        chart.rightAxis.drawGridLinesEnabled = false
        chart.xAxis.drawGridLinesEnabled = false
        
        // Axis Labels
        chart.xAxis.drawLabelsEnabled = true
        chart.xAxis.labelPosition = .bottom
        chart.xAxis.labelRotationAngle = 15
        chart.rightAxis.drawLabelsEnabled = false
        chart.xAxis.granularity = 1
        
        chart.leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: numberFormat)
        
        // Limit Line
        let intakeGoal = Double(UserDefaults.standard.integer(forKey: "intakeGoal"))/1000
        let goal = ChartLimitLine(limit: intakeGoal, label: "Goal")
        goal.lineWidth = CGFloat(1);
        goal.lineColor = limitColor
        goal.valueTextColor = limitColor
        goal.labelPosition = .leftBottom
        chart.leftAxis.addLimitLine(goal)
        chart.leftAxis.drawLimitLinesBehindDataEnabled = true
        
    }
    
    func updateChart(with drinks : [Drink])
    {
        values.removeAll()
        days.removeAll()
        
        let intakeGoal = Double(UserDefaults.standard.integer(forKey: "intakeGoal"))/1000
        chart.leftAxis.limitLines.first?.limit = intakeGoal
        
        var prevDate = Date()
        var xPos : Double = 0
        var value : Float = 0
        for drink in drinks
        {
            let currentDay = DateFormatter.localizedString(from: drink.date, dateStyle: .short, timeStyle: .none)
            // Sums up all values within a day
            if(days.isEmpty){
                days.append(currentDay)
                prevDate = drink.date
            }
            if(currentDay == days.last){
                value += drink.volume/1000
            } else{
                values.append(ChartDataEntry(x: xPos, y: Double(value)))
                value = drink.volume/1000
                
                // Insert data for days without entries
                let daysBetween = abs(prevDate.daysBetween(date: drink.date))
                if(daysBetween > 1){
                    for counter in 1..<daysBetween {
                        xPos += 1
                        let date =  prevDate.addingTimeInterval(60*60*24*Double(counter))
                        days.append(DateFormatter.localizedString(from: date, dateStyle: .short, timeStyle: .none))
                        values.append(ChartDataEntry(x: xPos, y: Double(0)))
                    }
                }
                
                xPos += 1
                prevDate = drink.date
                days.append(currentDay)
            }
            if(drinks.last == drink){
                values.append(ChartDataEntry(x: xPos, y: Double(value)))
            }
        }
        // Insert data for days without entries
        let daysBetween : Int = abs(prevDate.daysBetween(date: Date()))
        if(daysBetween > 1){
            for _ in 1..<daysBetween {
                xPos += 1
                values.append(ChartDataEntry(x: xPos, y: Double(0)))
            }
        }
        let dataset = LineChartDataSet(values: values, label: "Overview")
        
        // Removes the points and values
        dataset.drawCirclesEnabled = false
        dataset.drawFilledEnabled = true
        dataset.colors = [chartColor]
        dataset.fillColor = chartColor
        //dataset.fillAlpha = 1
        dataset.valueTextColor = chartColor
        dataset.drawValuesEnabled = false
        
        dataset.drawHorizontalHighlightIndicatorEnabled = true
        dataset.drawVerticalHighlightIndicatorEnabled = false
        
        // Smooth lines
        dataset.mode = .horizontalBezier
        let data = LineChartData(dataSet: dataset)
        data.setValueFormatter(DefaultValueFormatter(formatter : numberFormat))
        chart.data = data
        chart.xAxis.valueFormatter = IndexAxisValueFormatter(values: days)

    }
}
