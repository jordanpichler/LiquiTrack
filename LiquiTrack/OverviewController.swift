//
//  DataViewController.swift
//  LiquiTrack
//
//  Created by Student on 13/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import Foundation
import UIKit
import Charts

class OverviewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var tableView: UITableView!
    
    let cellIdentifier = "dataCell"
    let manager = DataManager.instance
    var chartManager : LineChartManager!
    var drinks = [[Drink]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drinks = manager.fetchDrinksGroupedByDay()
        tableView.reloadData()
        chartManager = LineChartManager.init(chart)
        chartManager.updateChart(with: manager.drinks)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        drinks = manager.fetchDrinksGroupedByDay()
        tableView.reloadData()
        chartManager.updateChart(with: manager.drinks)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        let drink = drinks[indexPath.section][indexPath.row]
        cell.textLabel?.text = drink.type!;
        if drink.volume < 1000 {
            cell.detailTextLabel?.text = "\(drink.volume) ml"
        }
        else{
            let volume = drink.volume/1000
            cell.detailTextLabel?.text = "\(volume) l"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return drinks[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return drinks[section][0].day
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return drinks.count
    }
}

