//
//  QuickDrinkViewController.swift
//  LiquiTrack
//
//  Created by Jordan Pichler on 05/03/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import UIKit

class QuickDrinkViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var quickDrinkPicker: UIPickerView!
    var touchOptionIdentifier: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.quickDrinkPicker.dataSource = self;
        self.quickDrinkPicker.delegate = self;
        
        let defaults = UserDefaults.standard
        
        if touchOptionIdentifier! == 1 {
            let typeIndex = defaults.integer(forKey: "typeLowIndex")
            let amountIndex = defaults.integer(forKey: "amountLowIndex")
            
            quickDrinkPicker.selectRow(typeIndex, inComponent: 1, animated: false)
            quickDrinkPicker.selectRow(amountIndex, inComponent: 0, animated: false)
            
        } else if touchOptionIdentifier! == 2 {
            let typeIndex = defaults.integer(forKey: "typeMediumIndex")
            let amountIndex = defaults.integer(forKey: "amountMediumIndex")
            
            quickDrinkPicker.selectRow(typeIndex, inComponent: 1, animated: false)
            quickDrinkPicker.selectRow(amountIndex, inComponent: 0, animated: false)
            
        } else if touchOptionIdentifier! == 3 {
            let typeIndex = defaults.integer(forKey: "typeHighIndex")
            let amountIndex = defaults.integer(forKey: "amountHighIndex")
            
            quickDrinkPicker.selectRow(typeIndex, inComponent: 1, animated: false)
            quickDrinkPicker.selectRow(amountIndex, inComponent: 0, animated: false)
        }
    
    }
    
    let pickerDataSource = [ [ "50 ml", "75 ml", "100 ml", "125 ml", "150 ml", "175 ml", "200 ml", "225 ml", "250ml", "275 ml", "300 ml", "400 ml", "500 ml", "600 ml", "700 ml", "750 ml", "800 ml", "900 ml", "1000 ml", "2000 ml"],
                             ["Water", "Coffee", "Milk", "Juice", "Beer", "Wine", "Softdrink", "Tea"] ]
    
    func numberOfComponents(in: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource[component].count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let defaults = UserDefaults.standard
        
        let amountStr = pickerDataSource[0][quickDrinkPicker.selectedRow(inComponent: 0)]
        let endIndex = amountStr.index(amountStr.endIndex, offsetBy: -3)
        let truncated = amountStr.substring(to: endIndex)
        let amount = Int(truncated)!
        print(amountStr)
        print(amount)
        
        let type = pickerDataSource[1][quickDrinkPicker.selectedRow(inComponent: 1)]
        print(type)

        if touchOptionIdentifier! == 1 {
            defaults.set(type, forKey: "typeLow")
            defaults.set(amount, forKey: "amountLow")
            
            defaults.set(quickDrinkPicker.selectedRow(inComponent: 1), forKey: "typeLowIndex")
            defaults.set(quickDrinkPicker.selectedRow(inComponent: 0), forKey: "amountLowIndex")


        } else if touchOptionIdentifier! == 2 {
            defaults.set(type, forKey: "typeMedium")
            defaults.set(amount, forKey: "amountMedium")
            
            defaults.set(quickDrinkPicker.selectedRow(inComponent: 1), forKey: "typeMediumIndex")
            defaults.set(quickDrinkPicker.selectedRow(inComponent: 0), forKey: "amountMediumIndex")


        } else if touchOptionIdentifier! == 3 {
            defaults.set(type, forKey: "typeHigh")
            defaults.set(amount, forKey: "amountHigh")
            
            defaults.set(quickDrinkPicker.selectedRow(inComponent: 1), forKey: "typeHighIndex")
            defaults.set(quickDrinkPicker.selectedRow(inComponent: 0), forKey: "amountHighIndex")

            
        }
    }
    
    
    
}
