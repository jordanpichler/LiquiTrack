//
//  QuietTimeViewController.swift
//  LiquiTrack
//
//  Created by Jordan Pichler on 05/03/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import UIKit

class QuietTimeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var quietPicker: UIPickerView!
    
    let pickerDataSource = [ ["from"],
                             ["12:00", "13:00", "14:00", "15:00","16:00", "17:00", "18:00", "19:00","20:00", "21:00", "22:00", "23:00", "00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00","08:00", "09:00", "10:00", "11:00"],
                             ["until"],
                             ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00","08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00","16:00", "17:00", "18:00", "19:00","20:00", "21:00", "22:00", "23:00"] ]
    
    var selectedFrom: Int {
        var value = UserDefaults.standard.integer(forKey: "quietFrom")
        
        if value >= 12 {
            value = value - 12
        } else {
            value = value + 12
        }
        
        return value
    }
    let selectedUntil = UserDefaults.standard.integer(forKey: "quietUntil")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.quietPicker.dataSource = self;
        self.quietPicker.delegate = self;
        
        quietPicker.selectRow(selectedFrom, inComponent: 1, animated: false)
        quietPicker.selectRow(selectedUntil, inComponent: 3, animated: false)
    }
    
    func numberOfComponents(in: UIPickerView) -> Int {
        return pickerDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource[component].count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[component][row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let defaults = UserDefaults.standard
        
        let fromTime = pickerDataSource[1][quietPicker.selectedRow(inComponent: 1)]
        let toTime = pickerDataSource[3][quietPicker.selectedRow(inComponent: 3)]
        print("\(fromTime) - \(toTime)")
        defaults.set("\(fromTime) - \(toTime)", forKey: "quietTime")
        
        var fromHour = quietPicker.selectedRow(inComponent: 1)
        if fromHour > 11 {
            fromHour = fromHour - 12
        } else {
            fromHour = fromHour + 12
        }
        let toHour = quietPicker.selectedRow(inComponent: 3)
        
        print("\(fromHour) - \(toHour)")
        
        defaults.set(fromHour, forKey: "quietFrom")
        defaults.set(toHour, forKey: "quietUntil")

    }
    
}
