//
//  Drink.swift
//  LiquiTrack
//
//  Created by Student on 14/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import Foundation
import CoreData

@objc(Drink)
public class Drink : NSManagedObject{
    var date : Date {
        get { return Date.init(timeIntervalSinceReferenceDate: timestamp) }
    }
    
    public var isToday : Bool {
        get {
            if(Calendar(identifier: .gregorian).isDateInToday(date)){
                return true
            }
            return false
        }
    }
    
    public var day : String {
        get {
            return DateFormatter.localizedString(from: date, dateStyle: .medium, timeStyle: .none)
        }
    }
}
