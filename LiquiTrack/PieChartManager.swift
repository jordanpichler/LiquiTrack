//
//  PieChartManager.swift
//  LiquiTrack
//
//  Created by Student on 18/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import Foundation
import Charts

class PieChartManager{
    var chart: PieChartView!
    var groupedDrinks = [String : Float]()
    let dataset = PieChartDataSet(values: nil, label: "Daily Intake")
    
    var goal : Float = 2000
    var sum : Float = 0
    
    // Defenition of the section colors
    let placeholderColor = UIColor(hex: 0xCCCCCC)
    let groupColors = [
        "Water" : UIColor(hex: 0x04BFBF),
        "Coffee" : UIColor(hex: 0xC77A47),
        "Tea" : UIColor(hex: 0xA9CF54),
        "Milk" : UIColor(hex: 0xFFEBB8),
        "Juice" : UIColor(hex: 0xFF950D),
        "Softdrink" : UIColor(hex: 0xCAFCD8),
        "Beer" : UIColor(hex: 0xFFCC55),
        "Wine" : UIColor(hex: 0xD94B3F)
    ]
    
    // Defenition of the number format
    var numberFormat : NumberFormatter {
        get{
            let pFormatter: NumberFormatter = NumberFormatter()
            pFormatter.numberStyle = NumberFormatter.Style.percent
            pFormatter.maximumFractionDigits = 2
            pFormatter.multiplier = 1.0
            pFormatter.percentSymbol = " l"
            return pFormatter
        }
    }
    
    init(_ chart : PieChartView){
        self.chart = chart
        chart.legend.enabled = false
        chart.chartDescription?.enabled = false
        
        // Removes the background grid
        chart.legend.drawInside = false
        
    }
    
    func updateChart(with drinks : [Drink], reset : Bool)
    {
        if(reset){
            dataset.resetColors()
            dataset.clear()
            groupedDrinks.removeAll()
            sum = 0
        }
        goal = Float(UserDefaults.standard.integer(forKey: "intakeGoal"))
        
        // Group the drinks by type
        for drink in drinks {
            if let group = groupedDrinks[drink.type!]
            {
                groupedDrinks[drink.type!] = group + drink.volume / 1000
            }else{
                groupedDrinks[drink.type!] = drink.volume / 1000
            }
        }
        
        // Create and configure the data set and add the grouped drinks to it
        for group in groupedDrinks{
            let entry = PieChartDataEntry(value: Double(group.value), label: group.key)
            dataset.addEntry(entry)
            if let color = groupColors[group.key] {
                dataset.addColor(color)
            }
            sum += group.value
        }
        
        // Adds the placeholder
        if(sum < goal/1000){
            dataset.addEntry(PieChartDataEntry(value: Double(goal/1000 - sum), label: "Missing"))
            dataset.addColor(placeholderColor)
        }
        
        dataset.valueFormatter = DefaultValueFormatter(formatter : numberFormat)
        
        let data = PieChartData()
        //data.setValueFormatter(DefaultValueFormatter(formatter : numberFormat))
        
        data.addDataSet(dataset)
        chart.data = data
        chart.centerText = "\(sum) l of \(goal / 1000) l"
    }
}
