//
//  AppDelegate.swift
//  LiquiTrack
//
//  Created by Jordan Pichler on 10/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "LiquiTrack")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: - 3D-Touch
    
    var launchedShortcutItem: UIApplicationShortcutItem?
    
    // MARK: Enum for identifying shortcut
    
    enum ShortcutIdentifier: String {
        case addDrinkLow
        case addDrinkMedium
        case addDrinkHigh
        var type: String {
            return Bundle.main.bundleIdentifier! + ".\(self.rawValue)"
        }
        
        init?(fullIdentifier: String) {
            guard let shortIdentifier = fullIdentifier.components(separatedBy: ".").last else {
                return nil
            }
            self.init(rawValue: shortIdentifier)
        }
    }
    // MARK: Handle Shortcut
    
    func handleShortcut(shortcutItem: UIApplicationShortcutItem) -> Bool {
        var handled = false
        print("in handleShortcut")
        
        guard ShortcutIdentifier(fullIdentifier: shortcutItem.type) != nil else {
            print("first guard kicked you")
            return false }
        guard let shortCutType = shortcutItem.type as String? else {
            print("second guard kicked you")
            return false }
        
        let defaults = UserDefaults.standard
        //TODO: Saving data to CoreDate goes here!!!
        switch (shortCutType) {
        case ShortcutIdentifier.addDrinkLow.type:
            handled = true
            if let type = defaults.string(forKey: "typeLow") {
                DataManager.instance.addDrinkOf(type: type, with: defaults.float(forKey: "amountLow"))
                print("Added \(type)")
            }
            
        case ShortcutIdentifier.addDrinkMedium.type:
            handled = true
            if let type = defaults.string(forKey: "typeMedium") {
                DataManager.instance.addDrinkOf(type: type, with: defaults.float(forKey: "amountMedium"))
                print("Added \(type)")
            }
            
        case ShortcutIdentifier.addDrinkHigh.type:
            handled = true
            if let type = defaults.string(forKey: "typeHigh") {
                DataManager.instance.addDrinkOf(type: type, with: defaults.float(forKey: "amountHigh"))
                print("Added \(type)")
            }
        default:
            print("Added nothing")
            break
        }
        
        // Construct & display alert (for testing on UI)
        let alertController = UIAlertController(title: "Shortcut Handled", message: "\"\(shortcutItem.localizedTitle)\"", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        window!.rootViewController?.present(alertController, animated: true, completion: nil)
        
        return handled
    }
    
    // MARK: -
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let defaults = UserDefaults.standard

        let launchedBefore = defaults.bool(forKey: "launchedBefore")
        let center = UNUserNotificationCenter.current()
        if launchedBefore  {
            print("Not first launch. Checking notifications authorization.")
            center.getNotificationSettings { (settings) in
                if settings.authorizationStatus != .authorized {
                    defaults.set(false, forKey: "notifcationsAllowed")
                    print("Notification authorization has been revoked!")
                } else {
                    print("Notification authorization OK")

                }
            }
        } else {
            print("First launch, setting UserDefaults and requesting Notifications")
            defaults.set(true, forKey: "launchedBefore")
            
            defaults.set("there", forKey: "userName")
            defaults.set(2000, forKey: "intakeGoal")

            defaults.set(2, forKey: "reminderInterval")
            defaults.set("21:00 - 07:00", forKey: "quietTime")
            defaults.set(21, forKey: "quietFrom")
            defaults.set(7, forKey: "quietUntil")

            defaults.set("Water", forKey: "typeLow")
            defaults.set("Coffee", forKey: "typeMedium")
            defaults.set("Juice", forKey: "typeHigh")
            
            defaults.set(150, forKey: "amountLow")
            defaults.set(300, forKey: "amountMedium")
            defaults.set(500, forKey: "amountHigh")
            
            defaults.set(0, forKey: "typeLowIndex")
            defaults.set(0, forKey: "typeMediumIndex")
            defaults.set(0, forKey: "typeHighIndex")
            
            defaults.set(4, forKey: "amountLowIndex")
            defaults.set(10, forKey: "amountMediumIndex")
            defaults.set(12, forKey: "amountHighIndex")
            
            let options: UNAuthorizationOptions = [.alert, .sound];
            center.requestAuthorization(options: options) {
                (granted, error) in
                if !granted {
                    print("Notifications denied")
                    defaults.set(false, forKey: "notifcationsAllowed")
                } else {
                    defaults.set(true, forKey: "notifcationsAllowed")
                }
            }


        }
        
        var shouldPerformAdditionalDelegateHandling = true
        
        //This if-statement blocks performActionFor to be called 2x (when app launches)
        if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            launchedShortcutItem = shortcutItem
            shouldPerformAdditionalDelegateHandling = false
        }
        
        //Retrieve UserDefaults for 3D-Touch Menu
        let typeLow = defaults.string(forKey: "typeLow")
        let typeMedium = defaults.string(forKey: "typeMedium")
        let typeHigh = defaults.string(forKey: "typeHigh")
        
        let amountLow = defaults.integer(forKey: "amountLow")
        let amountMedium = defaults.integer(forKey: "amountMedium")
        let amountHigh = defaults.integer(forKey: "amountHigh")
        
        //Define 3D-Touch shortcuts
        let shortcut1 = UIMutableApplicationShortcutItem(type: ShortcutIdentifier.addDrinkLow.type,
                                                         localizedTitle: "Log \(amountLow) ml",
                                                         localizedSubtitle: typeLow,
                                                         icon: UIApplicationShortcutIcon(type: .add),
                                                         userInfo: nil
        )
        
        let shortcut2 = UIMutableApplicationShortcutItem(type: ShortcutIdentifier.addDrinkMedium.type,
                                                         localizedTitle: "Log \(amountMedium) ml",
                                                         localizedSubtitle: typeMedium,
                                                         icon: UIApplicationShortcutIcon(type: .add),
                                                         userInfo: nil
        )
        
        let shortcut3 = UIMutableApplicationShortcutItem(type: ShortcutIdentifier.addDrinkHigh.type,
                                                         localizedTitle: "Log \(amountHigh) ml",
                                                         localizedSubtitle: typeHigh,
                                                         icon: UIApplicationShortcutIcon(type: .add),
                                                         userInfo: nil
        )
        
        application.shortcutItems = [shortcut1, shortcut2, shortcut3]
        
        return shouldPerformAdditionalDelegateHandling
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        print("in performActionFor")
        let handledShortCutItem = handleShortcut(shortcutItem: shortcutItem)
        completionHandler(handledShortCutItem)
        
    }
    
    
    
    
}
