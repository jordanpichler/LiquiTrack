//
//  ChartUtilities.swift
//  LiquiTrack
//
//  Created by Lisa on 14/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import Foundation
import UIKit
import Charts


// I'll replace this class later on
class Utilities{
    var entrySum : Float = 0
    
    var colors = [UIColor(hex: 0x04BFBF),
                  UIColor(hex: 0xCAFCD8),
                  UIColor(hex: 0xF7E967),
                  UIColor(hex: 0xA9CF54),
                  UIColor(hex: 0x588F27)]
    
    func initChart(_ chart : PieChartView, with title : String){
        chart.centerText = title
        
    }
    
    func generateColor() -> UIColor{
        let hue : CGFloat = CGFloat(arc4random_uniform(256))/255
        let brightness : CGFloat = CGFloat(185 + arc4random_uniform(50))/255
        let saturation : CGFloat = CGFloat(arc4random_uniform(200))/255
        
        let color = UIColor.init(hue: hue, saturation: saturation, brightness: brightness, alpha: CGFloat(1))
        print(color)
        return color
    }
    
    var numberFormat : NumberFormatter {
        get{
            let pFormatter: NumberFormatter = NumberFormatter()
            pFormatter.numberStyle = NumberFormatter.Style.percent
            pFormatter.maximumFractionDigits = 2
            pFormatter.multiplier = 1.0
            pFormatter.percentSymbol = " l"
            return pFormatter
        }
    }
}

extension UIColor {
    convenience init(hex: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green:CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha:  CGFloat(255 * alpha) / 255)
    }
}

extension Date {
    func dayNumberOfWeek() -> Int? {
        return Calendar.current.dateComponents([.weekday], from: self).weekday
    }
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized(with: Locale.current)
    }
    func daysBetween(date: Date) -> Int {
        return Date.daysBetween(start: self, end: date)
    }
    
    static func daysBetween(start: Date, end: Date) -> Int {
        let calendar = Calendar.current
        
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: start)
        let date2 = calendar.startOfDay(for: end)
        
        let a = calendar.dateComponents([.day], from: date1, to: date2)
        return a.value(for: .day)!
    }
}

