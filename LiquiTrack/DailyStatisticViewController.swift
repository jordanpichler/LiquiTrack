//
//  DailyStatisticViewController.swift
//  LiquiTrack
//
//  Created by Student on 14/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import Foundation
import UIKit
import Charts

class DailyStatisticController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    // Constants
    let cellIdentifier = "dataCell"
    let chartTitle = "Title"
    var chartManager : PieChartManager!
    
    // Outlets
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var tableView: UITableView!
    
    // Data
    let manager = DataManager.instance
    var todaysData = [Drink]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Initializes the tableview and chart
        todaysData = manager.fetchTodaysDrinks()
        chartManager = PieChartManager(pieChart)

        chartManager.updateChart(with: todaysData, reset: true)
        tableView.reloadData()
        tableView.backgroundColor = UIColor.clear

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        todaysData = manager.fetchTodaysDrinks()
        chartManager.updateChart(with: todaysData, reset: true)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        let drink = todaysData[indexPath.row]
        cell.textLabel?.text = drink.type!;
        if drink.volume < 1000 {
            cell.detailTextLabel?.text = "\(drink.volume) ml"
        }
        else{
            let volume = drink.volume/1000
            cell.detailTextLabel?.text = "\(volume) l"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return todaysData.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    /*
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Details"
    }
 */
}
