//
//  AddDrinkController.swift
//  LiquiTrack
//
//  Created by Jordan Pichler on 16/02/2017.
//  Copyright © 2017 Jordan A. Pichler. All rights reserved.
//

import UIKit

class AddDrinkController: UIViewController {
    
    @IBOutlet weak var DrinkImage: UIImageView!
    @IBOutlet var DrinkTypes: [UIButton]!
    @IBOutlet weak var DrinkAmount: UITextField!
    @IBOutlet weak var DrinkLabel: UILabel!
    let numberToolbar: UIToolbar = UIToolbar()


    @IBAction func onDrinkAdded(_ sender: UIButton) {
        if let volume = NumberFormatter().number(from: DrinkAmount.text!){
            print("Added \(Int(DrinkAmount.text!)!)ml of \(DrinkLabel.text!)")
            DataManager.instance.addDrinkOf(type: DrinkLabel.text!, with: volume.floatValue)
        }
    }
    
    @IBAction func onDrinkTypeChange(_ sender: UIButton) {
        DrinkLabel.text = sender.currentTitle
        DrinkImage.image = sender.image(for: .normal)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberToolbar.barStyle = UIBarStyle.blackTranslucent
        numberToolbar.items=[
            UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddDrinkController.cancelAmount)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddDrinkController.applyAmount))
        ]
        
        numberToolbar.sizeToFit()
        DrinkAmount.inputAccessoryView = numberToolbar
    }
    
    func applyAmount () {
        DrinkAmount.resignFirstResponder()
    }
    
    func cancelAmount () {
        DrinkAmount.text=""
        DrinkAmount.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
}
